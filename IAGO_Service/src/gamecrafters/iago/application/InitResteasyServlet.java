package gamecrafters.iago.application;

import gamecrafters.iago.resource.ImagesImplementation;
import gamecrafters.iago.resource.OntologyImpementation;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class InitResteasyServlet extends javax.ws.rs.core.Application{
	public Set<Class<?>> getClasses() {
		Set<Class<?>> s = new HashSet<Class<?>>();
		s.add(OntologyImpementation.class);
		s.add(ImagesImplementation.class);
		return s;
	}
}