package gamecrafters.iago.ontology.knowledge;

import gamecrafters.iago.internet.HttpClient;
import gamecrafters.iago.ontology.json.Concept;
import gamecrafters.iago.ontology.json.ConceptsArray;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class FreebaseGraphAdapter implements RemoteGraphAdapter {
	
	private static enum QueryType {
		QT_DOMAIN,
		QT_TYPE,
		QT_INSTANCE
	};
	
	private static final String QUERY_HEAD = "https://www.googleapis.com/freebase/v1/mqlread?query=";
	private static final String DOMAIN_QUERY = "[{" +
													"\"id\": null, " +
													"\"name\": null, " +
													"\"type\": \"/type/domain\", " +
													"\"!/freebase/domain_category/domains\": {" +
														"\"id\": \"/category/commons\" " +
													"} " +
												"}]";
	private static final String TYPE_QUERY = "[{" +
			"\"id\": null, " +
			"\"name\": null, " +
			"\"type\": \"/type/type\", " +
			"\"domain\": \"?\" " +
		"}]";
	private static final String INSTANCE_QUERY = "[{" +
			"\"id\": null, " +
			"\"name\": null, " +
			"\"type\": \"?\" " +
		"}]";
	
	@Override
	public ConceptsArray getSubconcepts(String parent, String sequenceType,
			long maxResults) {
		try {
			if(parent.length() <= 1) {
				return getConcepts(QueryType.QT_DOMAIN, parent, sequenceType, maxResults);
			} else {
				for (QueryType qt : QueryType.values()) {
					if(qt != QueryType.QT_DOMAIN) {
						ConceptsArray result = getConcepts(qt, parent, sequenceType, maxResults);
						if(result.getSize() > 0) {
							return result;
						}
					}
				}
			}
			
		} catch (JSONException jsonE) {
			jsonE.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ConceptsArray().withSize(0);
	}
	
	private String getQuery(QueryType type, String parent) throws UnsupportedEncodingException {
		String query = "";
		switch (type) {
		case QT_DOMAIN:
			query = QUERY_HEAD + URLEncoder.encode(DOMAIN_QUERY, "UTF-8");
			break;
		case QT_TYPE:
			query = QUERY_HEAD + 
					URLEncoder.encode(TYPE_QUERY.replace("?", parent), "UTF-8");
			//System.out.println("Query type: " + TYPE_QUERY.replace("?", parent));
			break;
		case QT_INSTANCE:
			query = QUERY_HEAD + 
					URLEncoder.encode(INSTANCE_QUERY.replace("?", parent), "UTF-8");
			//System.out.println("Inst query: " + INSTANCE_QUERY.replace("?", parent));
			break;
		}
		return query;
	}
	
	
	private ConceptsArray getConcepts(QueryType queryType, String parent, String sequenceType, 
			long maxResults) throws Exception
	{
		ConceptsArray conceptsArr = new ConceptsArray().withSize(0);
		List<Object> conceptsList = new ArrayList<Object>();
		String result = HttpClient.sendGet(getQuery(queryType, parent));
		if( (result.length() > 0) && (maxResults > 0) )
		{
			JSONObject json = new JSONObject(result);
			JSONArray resultItems = json.getJSONArray("result");
			SequenceGenerator sequence = new SequenceGenerator(sequenceType, maxResults);
			long i = sequence.getNextIndex(resultItems.length());
			while(i != -1) {
				final Concept concept = new Concept();
				concept.setName(resultItems.getJSONObject((int)i).getString("name"));
				concept.setUrl("https://www.freebase.com" + 
						resultItems.getJSONObject((int)i).getString("id"));
				conceptsList.add(concept);
				if(sequenceType.equals("random")) {
					resultItems.remove(resultItems.getJSONObject((int)i));
				}
				i = sequence.getNextIndex(resultItems.length());
			}
			conceptsArr.setSize(conceptsList.size());
			conceptsArr.setConcepts(conceptsList);
		}
		
		return conceptsArr;
	}
	
	
	private class SequenceGenerator {
		private String mSequenceType;
		private long mMaxResults;
		private long mIndex;
		private long mCount;
		
		
		public SequenceGenerator(String sequenceType,
				long maxResults) {
			mSequenceType = sequenceType;
			mMaxResults = maxResults;
			mIndex = 0;
			mCount = 0;
		}
		
		public long getNextIndex(long collectionSize) throws Exception {
			if(mCount >= mMaxResults || mCount >= collectionSize) {
				return -1;
			}
			switch(mSequenceType) {
				case "first":
				{
					++mIndex;
					++mCount;
					return mIndex-1;
				}
				case "last":
				{
					mIndex = (mIndex == 0 ? collectionSize - 1 : mIndex);
					--mIndex;
					++mCount;
					return mIndex+1;
				}
				case "random":
				{
					Random rand = new Random();
					mIndex = Math.abs(rand.nextLong()) % collectionSize;
					++mCount;
					return mIndex;
				}
				default:
				{
					throw new Exception("Unknown sequence type!");
				}
			}
		}
		
		public void reset()
		{
			mIndex = mCount = 0;
		}
	}
	

}
