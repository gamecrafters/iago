package gamecrafters.iago.ontology.knowledge;

import gamecrafters.iago.ontology.json.ConceptsArray;

public interface RemoteGraphAdapter {

	ConceptsArray getSubconcepts(String parent, String sequenceType, long maxResults);
	
}
