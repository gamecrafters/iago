package gamecrafters.iago.ontology.knowledge;

import gamecrafters.iago.ontology.json.Concept;

import java.util.List;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//ConceptsImpementation conImpl = new ConceptsImpementation();
		//System.out.println(conImpl.extractRelativeResourceUrl("https://www.freebase.com/music/artist"));
		
		FreebaseGraphAdapter graph = new FreebaseGraphAdapter();
		List<Object> result = graph.getSubconcepts("/education/educational_institution", "first", 20).getConcepts();
		for (int i = 0; i < result.size(); i++) {
			Concept item = (Concept)result.get(i);
			System.out.print("Name: " + item.getName() + ", ");
			System.out.println("Url: " + item.getUrl());
		}

	}

}
