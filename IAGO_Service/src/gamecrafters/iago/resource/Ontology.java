
package gamecrafters.iago.resource;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import gamecrafters.iago.json.Status;
import gamecrafters.iago.ontology.json.ConceptsArray;
import gamecrafters.iago.support.ResponseWrapper;

@Path("ontology")
public interface Ontology {


    /**
     * Retrieve a list of concepts from Freebase
     * 
     * @param sequenceType
     *     The sequence type of the max concepts returned from the query result - first/last/random e.g. random (random max results will be returned)
     * @param parent
     *     Uri of the parent concept used in the query. The response will be a set of concepts which are in a child relation with the parent concept. If not specified, the response will return all the root concepts from the knowledge base e.g. music (the response will contain the children concepts of the "music" concept)
     * @param maxResults
     *     The maximum number of concepts from the query result to be returned e.g. 10 (if the query result contains lets say 25 concepts and the sequence-type is "first", only the first 10 concepts will be returned)
     */
    @GET
    @Path("freebase/concepts")
    @Produces({
        "application/json"
    })
    Ontology.GetOntologyFreebaseConceptsResponse getOntologyFreebaseConcepts(
        @QueryParam("parent")
        String parent,
        @QueryParam("sequenceType")
        @NotNull
        String sequenceType,
        @QueryParam("maxResults")
        @NotNull
        long maxResults)
        throws Exception
    ;

    public class GetOntologyFreebaseConceptsResponse
        extends ResponseWrapper
    {


        private GetOntologyFreebaseConceptsResponse(Response delegate) {
            super(delegate);
        }

        /**
         *  e.g. {
         *   "data": {
         *     "size": 3
         *     "concepts": [
         *         {"url":"http://dbpedia.org/ontology/Mountain"},
         *         {"url":"http://dbpedia.org/ontology/Park"},
         *         {"url":"http://dbpedia.org/ontology/NaturalPlace"}
         *     ]
         *   },
         *   "success": true,
         *   "status": 200
         * }
         * 
         * 
         * @param entity
         *     {
         *       "data": {
         *         "size": 3
         *         "concepts": [
         *             {"url":"http://dbpedia.org/ontology/Mountain"},
         *             {"url":"http://dbpedia.org/ontology/Park"},
         *             {"url":"http://dbpedia.org/ontology/NaturalPlace"}
         *         ]
         *       },
         *       "success": true,
         *       "status": 200
         *     }
         *     
         */
        public static Ontology.GetOntologyFreebaseConceptsResponse withjsonOK(ConceptsArray entity) {
            Response.ResponseBuilder responseBuilder = Response.status(200).header("Content-Type", "application/json");
            responseBuilder.entity(entity);
            return new Ontology.GetOntologyFreebaseConceptsResponse(responseBuilder.build());
        }

        /**
         * Bad Request e.g. {
         *   "error": "Error message...",
         *   "success": false,
         *   "status": 400
         * }
         * 
         * 
         * @param status
         *     {
         *       "error": "Error message...",
         *       "success": false,
         *       "status": 400
         *     }
         *     
         */
        public static Ontology.GetOntologyFreebaseConceptsResponse withjsonBadRequest(gamecrafters.iago.json.Status status) {
            Response.ResponseBuilder responseBuilder = Response.status(400).header("Content-Type", "application/json");
            responseBuilder.entity(status);
            return new Ontology.GetOntologyFreebaseConceptsResponse(responseBuilder.build());
        }

        /**
         * Internal Server Error e.g. {
         *   "error": "Internal error message...",
         *   "success": false,
         *   "status": 500
         * }
         * 
         * 
         * @param status
         *     {
         *       "error": "Internal error message...",
         *       "success": false,
         *       "status": 500
         *     }
         *     
         */
        public static Ontology.GetOntologyFreebaseConceptsResponse withjsonInternalServerError(gamecrafters.iago.json.Status status) {
            Response.ResponseBuilder responseBuilder = Response.status(500).header("Content-Type", "application/json");
            responseBuilder.entity(status);
            return new Ontology.GetOntologyFreebaseConceptsResponse(responseBuilder.build());
        }

    }

}
