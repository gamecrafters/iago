package gamecrafters.iago.resource;

import gamecrafters.iago.ontology.json.Concept;
import gamecrafters.iago.ontology.json.ConceptsArray;
import gamecrafters.iago.ontology.knowledge.FreebaseGraphAdapter;
import gamecrafters.iago.ontology.knowledge.RemoteGraphAdapter;
import gamecrafters.iago.json.Status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

public class OntologyImpementation implements Ontology {

	@Override
	@GET
	@Path("ontology/freebase/concepts")
	@Produces("application/json")
	public
	GetOntologyFreebaseConceptsResponse getOntologyFreebaseConcepts(
			@QueryParam("parent") String parent,
			@QueryParam("sequenceType") @NotNull String sequenceType,
			@QueryParam("maxResults") @NotNull long maxResults)
			throws Exception {
		
		if( !sequenceType.equals("first") && 
				!sequenceType.equals("last") && 
				!sequenceType.equals("random")) {
			return GetOntologyFreebaseConceptsResponse.withjsonBadRequest(
					new Status().withError(
							"Bad Request: sequence type may contain only following values: " +
							"first/last/random"));
		}
		
		ConceptsArray conceptsArr = null;
		RemoteGraphAdapter graphAdapter = null;
		String graphUrl = extractGraphUrl(parent);
		String relativeResourceUrl = extractRelativeResourceUrl(parent);
		switch (graphUrl) {
			case "https://www.freebase.com":
			{
				graphAdapter = new FreebaseGraphAdapter();
				break;
			}
			case "https://www.test.com":
			{
				conceptsArr = new ConceptsArray();
				List<Object> conceptsList = new ArrayList<Object>();
				conceptsList.add(new Concept().withName("Test1").withUrl("url1"));
				conceptsList.add(new Concept().withName("Test2").withUrl("url2"));
				conceptsArr.setSize(conceptsList.size());
				conceptsArr.setConcepts(conceptsList);
				break;
			}
			default:
			{
				break;
			}
		}
		if (graphAdapter != null) {
			conceptsArr = graphAdapter.getSubconcepts(relativeResourceUrl, sequenceType, maxResults);
		}
		if (conceptsArr != null) {
			return GetOntologyFreebaseConceptsResponse.withjsonOK(conceptsArr);
		} else {
			return GetOntologyFreebaseConceptsResponse.withjsonInternalServerError(new Status().withError("Internal error!"));
		}
	}
	
	private String extractGraphUrl(String url)
	{
		String result = url;
		
		int index = url.indexOf('/', 8);
		if(index != -1) {
			result = url.substring(0, index);
		}
		return result;
	}
	
	private String extractRelativeResourceUrl(String url)
	{
		String result = "/";
		
		int index = url.indexOf('/', 8);
		if(index != -1) {
			result += url.substring(index+1, url.length());
		}
		return result;
	}

	
	
}
