package gamecrafters.iago.resource;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import gamecrafters.iago.images.json.Image;
import gamecrafters.iago.images.json.ImagesArray;
import gamecrafters.iago.internet.HttpClient;
import gamecrafters.iago.ontology.json.Concept;
import gamecrafters.iago.resource.Images;
import gamecrafters.iago.json.Status;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.PhotosInterface;

@Path("images")
public class ImagesImplementation implements Images {

	final static String FLICKR_API_KEY = "1c1aaf481de4f37da85248b81d6e4476";
	final static String FLICKR_SHARED_SECRET = "0f8201deaee8002b";
	
	final static String RDF_PREFIX = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ";
	final static String RDFS_PREFIX = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ";
	final static String FOAF_PREFIX = "PREFIX foaf:  <http://xmlns.com/foaf/0.1/> ";
	final static String FREEBASE_PREFIX = "PREFIX frbs: <https://www.freebase.com/> ";
	final static String DUBLIN_CORE_PREFIX = "PREFIX dc_terms: <http://purl.org/dc/terms/> ";
	final static String IAGO_IMAGE_PREFIX = "PREFIX iago_flickr_img: <http://localhost:8080/IAGO_Service/images/flickr/> ";
	
	final static String SPARQL_URI_BASE = "http://localhost:8890/sparql?default-graph-uri=http://iago.iago&format=json&timeout=0&debug=on&query=";
	final static String SPARQL_INSERT_START = "INSERT {";
	final static String SPARQL_INSERT_END = "}";
	
	final static String SPARQL_IMAGE_QUERY = "SELECT ?img_id ?img_url ?subject " +
											 "WHERE { " +
											 "	?img_id dc_terms:subject ?subject." +
											 "	?img_id dc_terms:source ?img_url." +
											 "	{ " +
											 "		SELECT ?img_id " +
											 "		WHERE { " +
											 "			?img_id dc_terms:subject [] " +
		    								 "		} " +
		    								 "		GROUP BY ?img_id " +
		    								 "		HAVING (COUNT(?img_id) >= #low && COUNT(?img_id) <= #high) " +
		  									 "	} " +
											 "} " +
											 "LIMIT #limit ";
	

	private ImagesArray getRandomFromFlickr(long amount) throws FlickrException {
		ImagesArray result = new ImagesArray();
		List<Object> imageList = new ArrayList<Object>();
		Flickr f = new Flickr(FLICKR_API_KEY, FLICKR_SHARED_SECRET, new REST());
		PhotosInterface phi = f.getPhotosInterface();
		PhotoList<Photo> phList = phi.getRecent(null, (int)amount, 0);
		for (Photo photo : phList) {
			imageList.add(new Image().withId(photo.getId())
										.withUrl(photo.getLargeUrl()));
		}
		result.setImages(imageList);
		return result;
	}

	private void registerImageSource(String imageId, String url) throws Exception {
		String query = SPARQL_URI_BASE + 
						URLEncoder.encode(DUBLIN_CORE_PREFIX +
						IAGO_IMAGE_PREFIX +
						SPARQL_INSERT_START + " " +
						"iago_flickr_img:" + imageId + " " +
						"dc_terms:source <" +
						url + "> " +
						SPARQL_INSERT_END,
						"UTF-8");
		String result = HttpClient.sendGet(query);
	}
	
	private void annotateImage(String imageId, String concept) throws Exception {
		String query = SPARQL_URI_BASE + 
						URLEncoder.encode(DUBLIN_CORE_PREFIX +
						IAGO_IMAGE_PREFIX +
						SPARQL_INSERT_START + " " +
						"iago_flickr_img:" + imageId + " " +
						"dc_terms:subject <" +
						concept + "> " +
						SPARQL_INSERT_END,
						"UTF-8");
		HttpClient.sendGet(query);
	}
	
	private ImagesArray getImagesFromTripleStore(int lowLimit, int highLimit, long amount) throws Exception {
		ImagesArray result = new ImagesArray();
		List<Object> imageList = new ArrayList<Object>();
		String query = SPARQL_URI_BASE + 
						URLEncoder.encode(DUBLIN_CORE_PREFIX +
						SPARQL_IMAGE_QUERY.replaceFirst("#low", Integer.toString(lowLimit))
										  .replaceFirst("#high", Integer.toString(highLimit))
										  .replaceFirst("#limit", Long.toString(1000)),
						"UTF-8");
		String jsonText = HttpClient.sendJsonGet(query);
		
		if( (jsonText.length() > 0) && (amount > 0) )
		{
			JSONObject json = new JSONObject(jsonText).getJSONObject("results");
			JSONArray resultItems = json.getJSONArray("bindings");
			int counter = 0;
			for (int i = 0; i < resultItems.length() && counter < amount; ++i) {
				JSONObject item = resultItems.getJSONObject(i);
				JSONObject imgId = item.getJSONObject("img_id");
				JSONObject imgUrl = item.getJSONObject("img_url");
				JSONObject subject = item.getJSONObject("subject");
				Image img = new Image().withId(imgId.getString("value"))
									   .withUrl(imgUrl.getString("value"));
				counter += insertImage(imageList, img, new Concept().withUrl(subject.getString("value")));
			}
			result.setSize(imageList.size());
			result.setImages(imageList);
		}
		return result;
	}

	private int insertImage(List<Object> imageList, Image img, Concept subject) {
		for (Object object : imageList) {
			Image item = (Image) object;
			if (item.getId().equals(img.getId())) {
				if (subject != null && subject.getUrl().length() > 0 ) {
					item.getSubjects().add(subject);
				}
				return 0;
			}
		}
		List<Object> conceptList = new ArrayList<Object>();
		if (subject != null && subject.getUrl().length() > 0) {
			conceptList.add(subject);
		}
		imageList.add( img.withSubjects(conceptList) );
		return 1;
	}
	
	@Override
	public GetImagesFlickrRandomResponse getImagesFlickrRandom(long amount,
			long rank) throws Exception {
		ImagesArray result= new ImagesArray().withSize(0);
		switch ((int)rank) {
			case 5:
			{
				result = getImagesFromTripleStore(7, 1000, amount);
			}
			case 4:
			{
				if (result != null && result.getSize() != 0) {
					break;
				}
				result = getImagesFromTripleStore(5, 6, amount);
			}
			case 3:
			{
				if (result != null && result.getSize() != 0) {
					break;
				}
				result = getImagesFromTripleStore(3, 4, amount);
			}
			case 2:
			{
				if (result != null && result.getSize() != 0) {
					break;
				}
				result = getImagesFromTripleStore(1, 2, amount);
			}
			case 1:
			{
				if (result != null && result.getSize() != 0) {
					break;
				}
				result = getRandomFromFlickr(amount);
				break;
			}
			
			default:
			{
				return GetImagesFlickrRandomResponse.withjsonBadRequest(new Status().withError("rank needs to be between 1 and 5."));
			}
		}
		return GetImagesFlickrRandomResponse.withjsonOK(result);
	}

	@Override
	public PostImagesFlickrByImageResponse postImagesFlickrByImage(
			String image, String url, String concept, String player)
			throws Exception {
		try {
			if ((url != null) && (url.length() > 0)) {
				registerImageSource(image, url);
			} else {
				throw new Exception("No image url...");
			}
			if ((concept != null) && (concept.length() > 0)) {
				annotateImage(image, concept);
			}
		} catch (Exception e) {
			PostImagesFlickrByImageResponse.withjsonInternalServerError(new Status().withError("Annotation failed!"));
		}
		return PostImagesFlickrByImageResponse.withjsonOK(new Status());
	}
}
