
package gamecrafters.iago.resource;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import gamecrafters.iago.images.json.ImagesArray;

@Path("images")
public interface Images {


    /**
     * Retrieve a number of random images
     * 
     * @param amount
     *     Number of random images to retrieve e.g. 3
     * @param rank
     *     Image annotation rank from 1 to 5 (5 stars data paradigm) e.g. 1
     */
    @GET
    @Path("flickr/random")
    @Produces({
        "application/json"
    })
    Images.GetImagesFlickrRandomResponse getImagesFlickrRandom(
        @QueryParam("amount")
        @DefaultValue("1")
        @Min(1L)
        @Max(500L)
        long amount,
        @QueryParam("rank")
        @DefaultValue("1")
        @Min(1L)
        @Max(5L)
        long rank)
        throws Exception
    ;

    /**
     * Annotate an image with a concept
     * 
     * @param concept
     *     Uri of the concept resource e.g. http://dbpedia.org/ontology/Mountain
     * @param player
     *     Uri of the player who annotates the image e.g. http://iago-service.org/players/Mike569
     * @param image
     *     
     * @param url
     *     Url of the concept resource e.g. http://flickr.com/img123.jpg
     */
    @POST
    @Path("flickr/{image}")
    @Produces({
        "application/json"
    })
    Images.PostImagesFlickrByImageResponse postImagesFlickrByImage(
        @PathParam("image")
        @NotNull
        String image,
        @QueryParam("url")
        String url,
        @QueryParam("concept")
        String concept,
        @QueryParam("player")
        String player)
        throws Exception
    ;

    public class GetImagesFlickrRandomResponse
        extends gamecrafters.iago.support.ResponseWrapper
    {


        private GetImagesFlickrRandomResponse(Response delegate) {
            super(delegate);
        }

        /**
         *  e.g. {
         *   "data": {
         *     "images": [
         *         {
         *           "id":"16244099909",
         *           "url":"https://farm8.static.flickr.com/7411/16244099909_17ac268e24_m.jpg"
         *         },
         *         {
         *           "id":"16244100409",
         *           "url":"https://farm8.static.flickr.com/7352/16244100409_ecca5839ed_m.jpg"
         *         },
         *         {
         *           "id":"16404377196",
         *           "url":"https://farm9.static.flickr.com/8585/16404377196_e280271a56_m.jpg"
         *         }
         *     ]
         *   },
         *   "success": true,
         *   "status": 200
         * }
         * 
         * 
         * @param entity
         *     {
         *       "data": {
         *         "images": [
         *             {
         *               "id":"16244099909",
         *               "url":"https://farm8.static.flickr.com/7411/16244099909_17ac268e24_m.jpg"
         *             },
         *             {
         *               "id":"16244100409",
         *               "url":"https://farm8.static.flickr.com/7352/16244100409_ecca5839ed_m.jpg"
         *             },
         *             {
         *               "id":"16404377196",
         *               "url":"https://farm9.static.flickr.com/8585/16404377196_e280271a56_m.jpg"
         *             }
         *         ]
         *       },
         *       "success": true,
         *       "status": 200
         *     }
         *     
         */
        public static Images.GetImagesFlickrRandomResponse withjsonOK(ImagesArray entity) {
            Response.ResponseBuilder responseBuilder = Response.status(200).header("Content-Type", "application/json");
            responseBuilder.entity(entity);
            return new Images.GetImagesFlickrRandomResponse(responseBuilder.build());
        }

        /**
         *  e.g. {
         *   "error": "Bad Request...",
         *   "success": false,
         *   "status": 400
         * }
         * 
         * 
         * @param status
         *     {
         *       "error": "Bad Request...",
         *       "success": false,
         *       "status": 400
         *     }
         *     
         */
        public static Images.GetImagesFlickrRandomResponse withjsonBadRequest(gamecrafters.iago.json.Status status) {
            Response.ResponseBuilder responseBuilder = Response.status(400).header("Content-Type", "application/json");
            responseBuilder.entity(status);
            return new Images.GetImagesFlickrRandomResponse(responseBuilder.build());
        }

        /**
         *  e.g. {
         *   "error": "Internal error message...",
         *   "success": false,
         *   "status": 500
         * }
         * 
         * 
         * @param entity
         *     {
         *       "error": "Internal error message...",
         *       "success": false,
         *       "status": 500
         *     }
         *     
         */
        public static Images.GetImagesFlickrRandomResponse withjsonInternalServerError(Status entity) {
            Response.ResponseBuilder responseBuilder = Response.status(500).header("Content-Type", "application/json");
            responseBuilder.entity(entity);
            return new Images.GetImagesFlickrRandomResponse(responseBuilder.build());
        }

    }

    public class PostImagesFlickrByImageResponse
        extends gamecrafters.iago.support.ResponseWrapper
    {


        private PostImagesFlickrByImageResponse(Response delegate) {
            super(delegate);
        }

        /**
         *  e.g. {
         *   "error": "",
         *   "success": true,
         *   "status": 200
         * }
         * 
         * 
         * @param status
         *     {
         *       "error": "",
         *       "success": true,
         *       "status": 200
         *     }
         *     
         */
        public static Images.PostImagesFlickrByImageResponse withjsonOK(gamecrafters.iago.json.Status status) {
            Response.ResponseBuilder responseBuilder = Response.status(200).header("Content-Type", "application/json");
            responseBuilder.entity(status);
            return new Images.PostImagesFlickrByImageResponse(responseBuilder.build());
        }

        /**
         *  e.g. {
         *   "error": "Error message...",
         *   "success": true,
         *   "status": 400
         * }
         * 
         * 
         * @param entity
         *     {
         *       "error": "Error message...",
         *       "success": true,
         *       "status": 400
         *     }
         *     
         */
        public static Images.PostImagesFlickrByImageResponse withjsonBadRequest(Status entity) {
            Response.ResponseBuilder responseBuilder = Response.status(400).header("Content-Type", "application/json");
            responseBuilder.entity(entity);
            return new Images.PostImagesFlickrByImageResponse(responseBuilder.build());
        }

        /**
         *  e.g. {
         *   "error": "Internal error message...",
         *   "success": true,
         *   "status": 500
         * }
         * 
         * 
         * @param status
         *     {
         *       "error": "Internal error message...",
         *       "success": true,
         *       "status": 500
         *     }
         *     
         */
        public static Images.PostImagesFlickrByImageResponse withjsonInternalServerError(gamecrafters.iago.json.Status status) {
            Response.ResponseBuilder responseBuilder = Response.status(500).header("Content-Type", "application/json");
            responseBuilder.entity(status);
            return new Images.PostImagesFlickrByImageResponse(responseBuilder.build());
        }

    }

}
