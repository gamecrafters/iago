package gamecrafters.iago.internet;

import java.net.URLEncoder;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


public class HttpClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			String result = sendGet("https://www.googleapis.com/freebase/v1/mqlread?query=" +
								URLEncoder.encode("[{\"id\": null, " +
									"\"name\": null, " +
									"\"type\": \"/type/domain\", " +
									"\"!/freebase/domain_category/domains\": { \"id\": \"/category/commons\" } " +
									"}]", "UTF-8")
							);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String sendGet(String url) throws Exception {
		String result = "";
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse response = httpclient.execute(httpGet);
            try {
                org.apache.http.HttpEntity entity = response.getEntity();
                result = IOUtils.toString(entity.getContent());
                EntityUtils.consume(entity);
            } finally {
                response.close();
            }
		} finally {
            httpclient.close();
        }
		
		return result;
	}
	
	public static String sendJsonGet(String url) throws Exception {
		String result = "";
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Content-Type", "application/json");
            CloseableHttpResponse response = httpclient.execute(httpGet);
            try {
                org.apache.http.HttpEntity entity = response.getEntity();
                result = IOUtils.toString(entity.getContent());
                EntityUtils.consume(entity);
            } finally {
                response.close();
            }
		} finally {
            httpclient.close();
        }
		
		return result;
	}
	
}
