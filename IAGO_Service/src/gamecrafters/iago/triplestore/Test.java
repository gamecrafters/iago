package gamecrafters.iago.triplestore;

import java.net.URLEncoder;

import gamecrafters.iago.internet.HttpClient;
import gamecrafters.iago.resource.ImagesImplementation;

public class Test {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		String query = "http://localhost:8890/sparql?default-graph-uri=http://test.org&format=json&timeout=0&debug=on&query=";
		String params = URLEncoder.encode("" +
						"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
						"SELECT ?res WHERE { ?res rdf:is <http://test.org/resource/image> } " +
						"LIMIT 100", "UTF-8");
		String params2 = "http://localhost:8890/sparql?default-graph-uri=http%3A%2F%2Ftest.org&query=prefix+rdf%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%3E%0D%0Aselect+distinct+%3FConcept+where+{%3FConcept+rdf%3Atype+[]}+LIMIT+100&should-sponge=&format=text%2Fhtml&timeout=0&debug=on";
		System.out.println(params);
		//String result = HttpClient.sendGet( query + params);
		//System.out.println(result.toString());
		//ImagesImplementation.registerImageSource("12345", "http://an_image.jpg");
		System.out.println(System.getProperty("catalina.base"));
	}

}
